#!/usr/bin/python3

#   Copyright (c) 2014 Tobias Leupold <tl at stonemx dot de>
#
#   vsgitam - very simple git access management
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the Free
#   Software Foundation in version 2 of the License.
#
#   This program is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#   or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

import sys
import os
import re
import configparser

def die(message):
    sys.stderr.write('vsgitam: {}\n'.format(message))
    sys.exit(1)

# We need exactly one parameter: the user name.
if len(sys.argv) != 2:
    die('Wrong number of command line parameters supplied.')

user = sys.argv[1]
home = os.environ.get('HOME')

# (Try to) Get the config
config = configparser.ConfigParser()
config.read(home + '/.vsgitam.ini')

# Get the requested ssh command
command = os.environ.get('SSH_ORIGINAL_COMMAND', None)
if command == None:
    die('Could not read SSH_ORIGINAL_COMMAND')
if '\n' in command:
    die('The command must not contain newlines.')

# We have to expand "~" manually
command = re.sub('~', home, command)

# The command should start with "git-"; Get the requested repository
match = re.match(r"^git-.+\s+'(.+)/*'", command)
if not match:
    die('Invalid command supplied.')
repository = match.group(1)

# Remove a possible trailing home in case an absolute path has been requested
repository = re.sub('^{}/'.format(home), '', repository)

if not repository in config:
    die('Repository "{}" is not configured. Can\'t grant access.'.format(repository))

if not 'allowed' in config[repository]:
    die('No allowed members for repository "{}" set. Can\'t grant access.'.format(repository))

if not user in config[repository]['allowed']:
    die('User "{0}" has no access to repository "{1}".'.format(user, repository))

# Start the actual git shell
os.execve('/usr/bin/git-shell', ['git-shell', '-c', command], {})

# This is only executed in case the above command failed.
die('Could not execute git-shell.')
