# vsgitam

## Minimalistic git access management

vsgitam implements a minimalistic access management for a git server. It's possible to grant write
access to a git repository to specific ssh keys.

## Setup/Usage

vsgitam can be "set up" as follows:

Put vsgitam somewhere you like, e. g. in the git user's home directory. Make sure it's executable
for the git user.

Configure ssh for your git user to only accept ssh public key authentication.

To grant access to a git repository to some user, add the authorized user's public ssh key to the
`~/.ssh/authorized_keys` of your git user in the following form (no line breaks):

    command="/path/to/vsgitam id_for_some_user",no-port-forwarding,
    no-X11-forwarding,no-agent-forwarding,no-pty [some_user's public key]

`id_for_some_user`, the user name passed to vsgitam, does not have to be the same one as the "real"
user name (but it can of course be). You can choose what you want here, so you can distinguish
multiple users with the same name and even with the same host name.

Add the user to `~/.vsgitam`:

    [some_repository.git]
    allowed = some_user

More users can simply be added separated by spaces, e. g.:

    [some_repository.git]
    allowed = some_user other_user

That's it :-)
